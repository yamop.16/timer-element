import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@polymer/paper-styles/paper-styles.js';

/**
 * `current-timer`
 * Component for show current timer.
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class CurrentTimer extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
        
        .horizontal {
          @apply --layout-horizontal;
        }
        
        .center {
          @apply --layout-center-justified;
        }
        
        .clock {
          @apply --paper-font-display2;
          color: var(--paper-grey-900);
        }
      </style>
      
      <div class="horizontal center">
        <span class="clock">{{time}}</span>
      </div>
    `;
  }
  
  static get properties() {
    return {
      time: {
        type: String,
        value: ''
      },
      local: {
        type: String,
        value: 'en-US'
      },
      options: {
        type: Object,
        value: {
          weekday: 'short', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric'
        }
      }
    };
  }
  
  ready(){
    super.ready();
    let time = setInterval(this._updateTime.bind(this), 1000);
  }
  
  _updateTime(){
    let d = new Date();
    this.time = d.toLocaleDateString(this.local, this.options);
  }
  
}

window.customElements.define('current-timer', CurrentTimer);
